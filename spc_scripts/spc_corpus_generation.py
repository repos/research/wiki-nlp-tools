import os
import urllib

import mwparserfromhell
from pyspark.sql import functions as F
from pyspark.sql import types as T

from mwtokenizer.config.symbols import NON_WHITESPACE_LANGUAGES_SPC_MAPPING as NWS
from mwtokenizer.tokenizer import Tokenizer

os.environ["PYSPARK_PYTHON"] = "/usr/lib/anaconda-wmf/bin/python3"
os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/lib/anaconda-wmf/bin/python3"
# .config('spark.driver.maxResultSize', '8G')
# .config('spark.executor.memoryOverhead', '8G' )
import wmfdata as wmf

spark = wmf.spark.create_custom_session(
    master="yarn",
    spark_config={
        "spark.driver.memory": "2g",
        "spark.dynamicAllocation.maxExecutors": 64,
        "spark.executor.memory": "8g",
        "spark.executor.cores": 4,
        "spark.sql.shuffle.partitions": 256,
    },
    ship_python_env=True,
)


def normalise_title(title):
    title = urllib.parse.unquote(title)
    title = title.strip()
    if len(title) > 0:
        title = title[0].upper() + title[1:]
    n_title = title.replace("_", " ")
    if "#" in n_title:
        n_title = n_title.split("#")[0]
    return n_title


def extract_article(row):
    """Extract the content of the article.
    normalize the titles"""
    return T.Row(
        pid=row.page_id,
        title=normalise_title(row.page_title),
        title_rd=normalise_title(row.page_redirect_title),
        wikitext=row.revision_text,
    )


def get_plain_text(row):
    """
    Parse the wikitext and return the plain text.
    """
    wikicode = row.wikitext
    try:
        text = mwparserfromhell.parse(wikicode).strip_code()  # made change
    except Exception as e:
        return ""
    return text


def sentence_tokenization(x, tokenizer):
    """
    Use the library's tokenizer class to tokenize the articles into sentences.
    """
    sentences = list(tokenizer.sentence_tokenize(x))
    return sentences


snapshot = "2022-11"
base_data_dir_hdfs = "spc_corpus"
base_data_dir_local = "/srv/home/appledora/sentencepiece/corpus/all"
base_temp_dir_local = "/srv/home/appledora/sentencepiece/corpus/"

for wiki_id in NWS.keys():
    lang_code = wiki_id.replace("wiki", "")
    tokenizer = Tokenizer(language_code=lang_code)

    wikipedia_all = (
        ## select table
        spark.read.table("wmf.mediawiki_wikitext_current")
        ## select wiki project
        .where(F.col("wiki_db") == wiki_id)
        .where(F.col("snapshot") == snapshot)
        ## main namespace
        .where(F.col("page_namespace") == 0)
        .where(F.col("revision_text").isNotNull())
        .where(F.length(F.col("revision_text")) > 0)
    )
    # print(wiki_id)

    output_hdfs_file = os.path.join(base_data_dir_hdfs, wiki_id)
    output_local_file = os.path.join(base_data_dir_local, wiki_id)
    output_local_file_tmp = os.path.join(base_temp_dir_local, "tmp", wiki_id)

    temp_df = wikipedia_all.rdd.map(extract_article).filter(lambda r: r is not None)
    if temp_df.isEmpty():
        print("temp_df is empty")
        continue
    wikipedia = spark.createDataFrame(temp_df)
    articles = wikipedia.where(F.col("title_rd") == "").select(
        "pid", "title", "wikitext"
    )
    chunks = articles.rdd.map(get_plain_text)
    if chunks.isEmpty():
        print("Chunks is empty")
        continue
    # TO-DO: I need to cross check this again
    mapped_rdd = chunks.map(lambda x: sentence_tokenization(x, tokenizer)).flatMap(
        lambda x: x
    )
    os.system("hadoop fs -rm -r %s" % output_hdfs_file)
    # save rdd object in the hadoop file system
    mapped_rdd.saveAsTextFile(
        output_hdfs_file,
        compressionCodecClass="org.apache.hadoop.io.compress.GzipCodec",
    )

    # copy the hadoop file to local file system
    os.system(
        "hadoop fs -copyToLocal %s %s" % (output_hdfs_file, output_local_file_tmp)
    )
    # concatenate and unzip into single file.The number of lines is limited to 30M
    os.system(
        "cat %s/*| gunzip -c | head -n 30000000  > %s"
        % (output_local_file_tmp, output_local_file + ".txt")
    )
    # remove set of tmp-dirs
    os.system("rm -rf %s" % output_local_file_tmp)
    # remove hadoop data
    os.system("hadoop fs -rm -r %s" % output_hdfs_file)
