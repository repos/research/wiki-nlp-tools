import glob

import sentencepiece as spm

path = r"../corpus/all/*.txt"  # saved corpus directory
files = list(glob.glob(path))

# a .model and .vocab file will be saved in the current directory
# change the training parameters using spc repo documentation
# https://github.com/google/sentencepiece/blob/master/doc/options.md

# for this example, we are using a byte pair encoding model.
# Other options are unigram, char, or word
mtype = "bpe"
spm.SentencePieceTrainer.train(
    input=files, model_prefix="nonspace_all_bpe", vocab_size=50000, model_type=mtype
)
