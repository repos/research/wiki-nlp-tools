from .tokenizer import Tokenizer

__title__ = "mwtokenizer"
__summary__ = """mwtokenizer is a language-agnostic NLP toolkit for processing Wikipedia
articles with adequate performance out-of-the-box."""
__url__ = "https://gitlab.wikimedia.org/repos/research/wiki-nlp-tools/"

__version__ = "0.2.0"

__license__ = "MIT License"

__all__ = ["Tokenizer"]
