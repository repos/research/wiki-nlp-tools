import argparse
import json
import os
from random import Random

import pandas as pd

from mwtokenizer.config.symbols import SENTENCE_TERMINATORS_WITH_NO_SPACE
from mwtokenizer.tokenizer import Tokenizer

# The json contains manually sampled sentences from the FLORES101 dataset
# Currently, we have sampled sentences for three languages: English, German, and Bangla
# See: https://github.com/facebookresearch/flores for more information
__dir__ = os.path.dirname(__file__)
TEST_FILE_PATH = os.path.join(__dir__, "assets", "sentence_bmark.json")


def deterministic_benchmark(
    use_abbreviation: bool = True,
    benchmark_output_directory: str = "",
    benchmark_log_directory: str = "",
    shuffle_seed: int = 42,
) -> None:
    # add documentation in sphinx format
    """
    This is a deterministic benchmarking function that takes input from the
    dataset sample in TEST_FILE_PATH and calculates the performance of the algorithm.
    The TEST_FILE_PATH contains manually sampled sentences from the FLORES101 dataset.
    Pairs of sentences are concatenated randomly and passed to the Sentence Tokenizer.
    The tokenizer is expected to split the concatenated sentence into two sentences.
    When the split perfectly matches the input sentences, it is considered to be correct.
    When the split matches one of the input sentences, it is considered to be partially correct.

    The result of the benchmarking is stored in a csv file at the benchmark_output_directory.
    If the benchmark_output_directory is not specified, the result is stored in the current working directory.
    :params use_abbreviation_: whether to apply the abbreviation post processing
    :type use_abbreviation_: bool
    :params benchmark_output_directory: the directory where the benchmarking results would be stored.
    :type benchmark_output_directory: str
    :params shuffle_seed: the seed used to shuffle the sentences in the dataset
    :type shuffle_seed: int


    :return: None
    :rtype: None
    """
    with open(TEST_FILE_PATH, "r") as fin:
        test_sentences_file = json.load(fin)
    languages = sorted(list(test_sentences_file.keys()))
    abbr_status = "abbr" if use_abbreviation else "noabbr"

    output_file_name = f"sentence_benchmark_output_s{shuffle_seed}_{abbr_status}.tsv"
    log_file_name = f"sentence_benchmark_log_s{shuffle_seed}_{abbr_status}.tsv"
    output_file_path = os.path.join(benchmark_output_directory, output_file_name)
    log_file_path = os.path.join(benchmark_log_directory, log_file_name)
    # We can identify four types of errors:
    # type 1 (2-no-match): splits into two sentences. But neither the input sentences
    # type 2 (>2-one-match): splits into more than two sentences, with at least one of the sentences
    # type 3 (>2-no-match): splits into more than two sentences, with none of the sentences
    # type 4 (no-split): doesn't split into two sentences
    benchmark_error_list = []
    benchmark_df_list = []
    for language in languages:
        sentence_samples = test_sentences_file[language]
        # shuffle the sentences tointroduce randomness
        # having a shuffle seed ensures that the results are reproducible
        Random(shuffle_seed).shuffle(sentence_samples)
        tokenizer = Tokenizer(language_code=language)

        # append the first sentence to the end of the list to ensure complete
        # sentence coverage when comparing pairs of consecutive sentences
        sentence_samples.append(sentence_samples[0])
        # compare the split between the consecutive sentences using sentence_splitter_algorithm
        n_boundaries = len(sentence_samples) - 1  # number of expected boundaries
        n_correct = 0  # number of correct boundaries identified by the algorithm
        n_partial_correct = (
            0  # the number of examples where at least one of the sentences
        )
        # outputted from the tokenizer match the input sentences
        n_incorrect = 0  # the number of examples where the segments outputted from
        # the tokenizer doesn't match either of the input sentences
        n_missing = 0  # the number of examples where no segments are outputted from the tokenizer

        for i in range(len(sentence_samples) - 1):
            current_sentence = sentence_samples[i]
            next_sentence = sentence_samples[i + 1]
            sentence_joiner = (
                ""
                if current_sentence[-1] in SENTENCE_TERMINATORS_WITH_NO_SPACE
                else " "
            )
            concatenated_sentence = sentence_joiner.join(
                [current_sentence, next_sentence]
            )
            split_sentences = list(
                tokenizer.sentence_tokenize(
                    concatenated_sentence,
                    use_abbreviation=use_abbreviation,
                )
            )

            if len(split_sentences) == 2:
                # we add an extra space to the end of the first sentence
                # because we added a space between the two sentences
                # when we concatenated them
                if (
                    split_sentences[0] == current_sentence + sentence_joiner
                    and split_sentences[1] == next_sentence
                ):
                    n_correct += 1
                else:
                    n_incorrect += 1
                    # add the incorrect sentence to the log csv file
                    benchmark_error_list.append(
                        {
                            "language": language,
                            "sentence_1": current_sentence,
                            "sentence_2": next_sentence,
                            "split_sentences": split_sentences,
                            "type": "2-no-match",
                        }
                    )

            else:
                # there can be two cases, more than 2 sentences or less than 2 sentences are generated
                # if more than 2 sentences, we have to count, how many sentences are exactly matching
                if len(split_sentences) > 2:
                    # check if any of the sentences exactly match the input sentences
                    if (
                        current_sentence in split_sentences
                        or next_sentence in split_sentences
                    ):
                        n_partial_correct += 1
                        benchmark_error_list.append(
                            {
                                "language": language,
                                "sentence_1": current_sentence,
                                "sentence_2": next_sentence,
                                "split_sentences": split_sentences,
                                "type": ">2-one-match",
                            }
                        )
                    else:
                        n_incorrect += 1
                        benchmark_error_list.append(
                            {
                                "language": language,
                                "sentence_1": current_sentence,
                                "sentence_2": next_sentence,
                                "split_sentences": split_sentences,
                                "type": ">2-no-match",
                            }
                        )
                else:
                    n_missing += 1
                    benchmark_error_list.append(
                        {
                            "language": language,
                            "sentence_1": current_sentence,
                            "sentence_2": next_sentence,
                            "split_sentences": split_sentences,
                            "type": "no-split",
                        }
                    )

        # calculate accuracy
        accuracy = n_correct / n_boundaries
        # save the information in a pandas dataframe
        temp_df = {
            "Language": language,
            "Number of Samples": n_boundaries,
            "Correct": n_correct,
            "Partially Correct": n_partial_correct,
            "Incorrect": n_incorrect,
            "Missing": n_missing,
            "Accuracy": accuracy,
        }
        benchmark_df_list.append(temp_df)

        assert (
            n_boundaries == n_correct + n_partial_correct + n_incorrect + n_missing
        ), (
            "The number checked samples (correct+partially correct+incorrect+missing)"
            " does not match the total number of samples in the dataset."
        )
    benchmark_df = pd.DataFrame(benchmark_df_list)
    benchmark_df.to_csv(output_file_path, index=False, sep="\t")
    print(f"Results saved to {os.path.abspath(output_file_path)}")

    # save the error log to a csv file
    benchmark_error_df = pd.DataFrame(benchmark_error_list)
    benchmark_error_df.to_csv(log_file_path, index=False, sep="\t")
    print(f"Error log saved to {os.path.abspath(log_file_path)}")


# create command line interface
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--use_abbreviations",
        action="store_true",
        help="Whether to consider abbreviations during tokenization.",
    )
    parser.add_argument(
        "--output_directory",
        type=str,
        default="results/",
        help="The directory where the benchmarking results would be stored.",
    )

    parser.add_argument(
        "--log_directory",
        type=str,
        default="logs/",
        help="The directory where the benchmarking logs would be stored.",
    )
    parser.add_argument(
        "--seed",
        type=int,
        default=42,
        help="The seed used to shuffle the sentences in the dataset",
    )
    args = parser.parse_args()
    deterministic_benchmark(
        use_abbreviation=args.use_abbreviations,
        benchmark_output_directory=args.output_directory,
        benchmark_log_directory=args.log_directory,
        shuffle_seed=args.seed,
    )
