import argparse
import gzip
import json
import os
from typing import List, Text, Tuple

import pandas as pd

from mwtokenizer.tokenizer import Tokenizer

__dir__ = os.path.dirname(__file__)
TEST_FILE_PATH = os.path.join(__dir__, "assets", "word_bmark.json.gz")


# Example
"""
https://ja.wikipedia.org/wiki/5%E4%B8%96%E7%B4%80
Input sentence: "日本では倭国の古墳時代中期にあたる。"
Wikitext: [[日本]]では[[倭国]]の[[古墳時代]]中期にあたる
There are
Ideal output: ["日本", "倭国", "古墳時代"] = 3 tokens
Acceptable but trivial output: ["日", "本", "倭", "国", "古", "墳", "時", "代"] = 8 tokens
Unacceptable outputs, things like: "本で" or "時代中期"
"""


def calculate_scores(
    number_of_anchor_chars: int,
    number_of_anchors: int,
    number_of_boundaries: int,
    tokens_to_cover_anchors: int,
    boundary_cross: int,
) -> Tuple[float, float, float]:
    """
    We use the following definitions:
    Precisions = the proportion of tokens that fall within the anchors. Ideally, we don't want any tokens to cross boundaries of anchors.
    Recall = how are the anchors covered by the tokens. Ideally, for NWS languages we want each link to be a token.
    F1 = the harmonic mean of precision and recall.
    """
    range_of_scores = number_of_anchor_chars - number_of_anchors
    precision = 1 - (boundary_cross / number_of_boundaries)
    recall = 1 - ((tokens_to_cover_anchors - number_of_anchors) / range_of_scores)
    f1 = 2 * (precision * recall) / (precision + recall)
    return precision, recall, f1


def count_boundaries(text, anchors_spans_list) -> int:
    """
    Calculate the number of boundaries created by the anchors.
    """
    boundary_count = 0
    for anchor_span in anchors_spans_list:
        start, end = anchor_span
        # if an anchor is at the start of the text, count 1 boundary
        if start == 0:
            boundary_count += 1
        # if an anchor is at the end of the text, count 1 boundary
        elif end == len(text) - 1:
            boundary_count += 1
        # if an anchor is in the middle of the text, count 2 boundaries
        else:
            boundary_count += 2
    return boundary_count


def get_substring_spans(string: Text, substrings: List[Text]) -> List[Tuple[int, int]]:
    token_spans = []
    offset = 0
    for substring in substrings:
        # find the first occurrence of the token in the text, get the span, calculate the offset and repeat
        start = string.find(substring, offset)
        if start == -1:
            continue
        end = start + len(substring)
        token_spans.append((start, end - 1))
        offset = start
    return token_spans


def get_adjacent_tokens(
    token_spans_list: List[Tuple[int, int]], token_id: int, adj_range: int, text: str
) -> List[str]:
    """
    Given the index of a token, extract the adjacent tokens from the text.
    """
    # get the preceding `range` number of tokens from tokens_spans_list
    adjacent_tokens: List[str] = []
    token_start, token_end = token_spans_list[token_id]
    adjacent_tokens.append(f"<<{text[token_start:token_end+1]}>>")
    for i in range(1, adj_range + 1):
        if token_id - i >= 0:
            adjacent_token_start, adjacent_token_end = token_spans_list[token_id - i]
            adjacent_tokens.insert(
                0, text[adjacent_token_start : adjacent_token_end + 1]
            )

        if token_id + i < len(token_spans_list):
            adjacent_token_start, adjacent_token_end = token_spans_list[token_id + i]
            adjacent_tokens.append(text[adjacent_token_start : adjacent_token_end + 1])

    return adjacent_tokens


# write a function to check if token indices overlap with anchor indices
def check_overlap(
    token_spans_list: List[Tuple[int, int]],
    anchor_spans_list: List[Tuple[int, int]],
    text: str,
    sample_id: int,
    sample_raw_text: str,
    language: str,
):
    """
    To check overlaps, we use the following rules:
    1. If the token is a subset of the anchor, count 1 overlap.
    2. If the token goes over both ends of the anchor, count 1 boundary.
    3. If the token goes over one end of the anchor, count 1 boundary.
    4. If the token is not overlapping or crossing boundaries with the anchor, ignore it.
    """
    overlap_count, boundary_cross_count = 0, 0
    unit_error_list = []
    for token_id, token_spans in enumerate(token_spans_list):
        token_start, token_end = token_spans
        token = text[token_start : token_end + 1]
        for anchor_start, anchor_end in anchor_spans_list:
            anchor = text[anchor_start : anchor_end + 1]
            if token_start >= anchor_start and token_end <= anchor_end:
                overlap_count += 1
                break
            # check if the token falls outside the anchor on either ends
            elif token_start < anchor_start and token_end >= anchor_start:
                boundary_cross_count += 1
                # get adjacent tokens of the current token
                unit_error_item = {
                    "language": language,
                    "sentence_id": sample_id,
                    "sample_raw_text": sample_raw_text,
                    "token": token,
                    "anchor": anchor,
                    "adjacent_tokens": get_adjacent_tokens(
                        token_spans_list, token_id, len(anchor) - 1, text
                    ),
                }
                unit_error_list.append(unit_error_item)
                break
            elif token_start <= anchor_end and token_end > anchor_end:
                boundary_cross_count += 1
                unit_error_item = {
                    "language": language,
                    "sentence_id": sample_id,
                    "sample_raw_text": sample_raw_text,
                    "token": token,
                    "anchor": anchor,
                    "adjacent_tokens": get_adjacent_tokens(
                        token_spans_list, token_id, len(anchor) - 1, text
                    ),
                }
                unit_error_list.append(unit_error_item)
                break

            else:
                continue

    return overlap_count, boundary_cross_count, unit_error_list


def deterministic_benchmark_word(
    use_abbreviation: bool,
    benchmark_output_directory: str = "",
    benchmark_log_directory: str = "",
    output_suffix: str = "",
) -> None:
    """
    This is a deterministic benchmarking function that takes input from the
    dataset sample in TEST_FILE_PATH and calculates the performance of the word tokenization algorithm.
    The dataset is generated from wikipedia.
    We use link anchors as potential ground truth tokens. The algorithm is considered correct if
    the tokens are a subset of the anchors. We ignore all tokens that are not overlapping or crossing boundaries
    with the anchors. We also ignore all tokens that are not in the same order as the anchors, by using the
    indices of the anchors for reference.
    We then calculate the precision, recall and f1 scores for the algorithm.
    """
    with gzip.open(TEST_FILE_PATH, "rt") as fin:
        test_bmark_file = json.load(fin)
    # The dataset is a json file with the following structure:
    # {
    #     "language_code": [
    #         {
    #             "text": "text",
    #             "anchors": [ {"text": "anchor", "start": start, "end": end}, ... ]
    languages = sorted(test_bmark_file.keys())
    abbr_status = "abbr" if use_abbreviation else "noabbr"
    output_file_name = f"word_benchmark_output_{abbr_status}_{output_suffix}.tsv"
    log_file_name = f"word_benchmark_log_{abbr_status}_{output_suffix}.tsv"
    output_file_path = os.path.join(benchmark_output_directory, output_file_name)
    log_file_path = os.path.join(benchmark_log_directory, log_file_name)
    benchmark_df_list = []
    benchmark_error_list = []
    for language in languages:
        sample_list = test_bmark_file[language]
        tokenizer = Tokenizer(language_code=language)
        number_of_anchors = 0
        number_of_boundaries = 0
        number_of_anchor_chars = 0
        overlap_count = 0
        boundary_cross_count = 0
        for sample in sample_list:
            sample_id = sample["id"]
            sample_text = sample["text"]
            sample_raw_text = sample["raw"]
            sample_anchors = [anchor["text"] for anchor in sample["anchors"]]
            if len(sample_anchors) == 0:
                continue
            tokens = list(
                tokenizer.word_tokenize(sample_text, use_abbreviation=use_abbreviation)
            )
            tokens = [token.strip() for token in tokens]
            # remove empty/whitespace tokens
            tokens = [token for token in tokens if len(token) > 0]

            # find span indices of anchors and tokens
            anchor_spans_list = [
                (anchor["start"], anchor["end"]) for anchor in sample["anchors"]
            ]
            token_spans_list = get_substring_spans(sample_text, tokens)
            # calculate the number of boundaries created by the anchors
            sample_number_of_boundaries = count_boundaries(
                sample_text, anchor_spans_list
            )
            # calculate the number of anchor characters - to define the worst case scenario of tokenization
            # also strip the whitespace characters from the anchors
            sample_number_of_anchor_chars = sum(
                [len(anchor.replace(" ", "")) for anchor in sample_anchors]
            )
            (
                sample_overlap_count,
                sample_boundary_cross_count,
                sample_unit_error_list,
            ) = check_overlap(
                token_spans_list,
                anchor_spans_list,
                sample_text,
                sample_id,
                sample_raw_text,
                language,
            )
            number_of_anchors += len(anchor_spans_list)
            number_of_boundaries += sample_number_of_boundaries
            number_of_anchor_chars += sample_number_of_anchor_chars
            overlap_count += sample_overlap_count
            boundary_cross_count += sample_boundary_cross_count
            benchmark_error_list.extend(sample_unit_error_list)

        precision, recall, f1 = calculate_scores(
            number_of_anchor_chars,
            number_of_anchors,
            number_of_boundaries,
            overlap_count + boundary_cross_count,
            boundary_cross_count,
        )

        benchmark_df_list.append(
            {
                "language": language,
                "precision": precision,
                "recall": recall,
                "f1": f1,
            }
        )
    benchmark_df = pd.DataFrame(benchmark_df_list)
    benchmark_df.to_csv(output_file_path, index=False, sep="\t")
    print(f"Saved benchmark results to {output_file_path}")

    # logging support to be added
    benchmark_error_df = pd.DataFrame(benchmark_error_list)
    benchmark_error_df.to_csv(log_file_path, index=False, sep="\t")
    print(f"Saved benchmark errors to {log_file_path}")


# create command line interface
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--use_abbreviations",
        action="store_true",
        help="Whether to consider abbreviations during tokenization.",
    )
    parser.add_argument(
        "--output_directory",
        type=str,
        default="results/",
        help="The directory where the benchmarking results would be stored.",
    )
    parser.add_argument(
        "--log_directory",
        type=str,
        default="logs/",
        help="The directory where the benchmarking logs would be stored.",
    )
    parser.add_argument(
        "--output_suffix",
        type=str,
        default="all",
        help="The suffix to be added to the output file names.",
    )

    args = parser.parse_args()
    deterministic_benchmark_word(
        use_abbreviation=args.use_abbreviations,
        benchmark_output_directory=args.output_directory,
        benchmark_log_directory=args.log_directory,
        output_suffix=args.output_suffix,
    )
